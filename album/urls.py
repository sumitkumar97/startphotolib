from django.urls import path
from .views import *

urlpatterns = [
    path('add', FileUploadView.as_view()),
    path('create', AlbumView.as_view()),
    path('list/private',AlbumListPrivate.as_view()),
    path('list/public',AlbumListPublic.as_view()),
    path('<int:album_id>/images',AlbumImageList.as_view()),
    path('delete/<int:album_id>',AlbumDeleteView.as_view()),
    path('delete/image/<int:image_id>',ImageDeleteView.as_view())
]